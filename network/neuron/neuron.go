package network

import (
	activation "deeplearning/activation/sigmoid"
)

func Neuron(values ...float64) float64 {
	total := 0.0

	for _, value := range values {
		total += value
	}

	return activation.Sigmoid(total)
}
package network

func WeightSum(activation float64, weight float64) float64 {
	return activation * weight
}
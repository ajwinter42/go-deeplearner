package activation

import "math"

func Relu(input float64) (output float64) {
	return math.Max(0, input)
}

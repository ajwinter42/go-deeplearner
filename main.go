package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
)

type Network struct {
	Neurons [][]float64 `json:"neurons"`
	Weights [][][]float64   `json:"weights"`
	Score int `json:"score"`
}

type Networks []Network

func main() {
	var input *bufio.Scanner = bufio.NewScanner(os.Stdin)

	for input.Scan() {
		//var inputRaw []string = strings.Split(input.Text(), " ")
		//words := rememberWords(inputRaw)

		var existingNetworks Networks
		data, _ := ioutil.ReadFile("networks.json")
		json.Unmarshal([]byte(data), &existingNetworks)

		var network Network = randomNetwork()

		existingNetworks = append(existingNetworks, network)

		file, _ := json.MarshalIndent(existingNetworks, "", " ")
 
		ioutil.WriteFile("networks.json", file, 0644)
	}
}

func randomFloat(min int, max int) float64 {
	return float64(randomInt(min, max))
}

func randomInt(min int, max int) int {
	return rand.Intn(max - min) + min
}

func randomNetwork() Network {
	var network Network

	const min int = 10
    const max int = 30
	
	for a := 0; a < randomInt(min, max); a++ {
		var newLayer [][]float64
		var newNeuronLayer []float64 
		const bmin int = 10
		const bmax int = 30

		var neuronCount int = randomInt(bmin, bmax)

		for b := 0; b < neuronCount; b++ {
			const cmin int = 10
			const cmax int = 30
			var clength int = randomInt(cmin, cmax)

			newNeuronLayer = append(newNeuronLayer, randomFloat(cmin, cmax))

			weights := make([]float64, clength)
			for c := 0; c <= clength; c++ {
				weights = append(weights, randomFloat(-10, 10))
				fmt.Print(fmt.Sprint(a, b, c) + "  -  ")
			}
			newLayer = append(newLayer, weights)
		}

		network.Neurons = append(network.Neurons, newNeuronLayer)
		network.Weights = append(network.Weights, newLayer)
	}
	
	network.Score = 0
	
	return network
}

func rememberWords(inputRaw []string) map[string]bool {
	inputSet := make(map[string]bool)
	existingWords := make(map[string]bool)

	for _, word := range inputRaw {
		inputSet[word] = true
	}

	data, _ := ioutil.ReadFile("words.json")

    json.Unmarshal([]byte(data), &existingWords)

	for existing := range existingWords {
		inputSet[existing] = true
	}

	file, _ := json.MarshalIndent(inputSet, "", " ")
 
	ioutil.WriteFile("words.json", file, 0644)

	return inputSet
}